This program connects your laptop/desktop with Bluetooth support to your Amazfit smartwatch and does the following supported operations.

Note: This program works only with Python version >= 3.7.6, because of the usage of asyncio

### Supported Operations

1. Read Device Name & Device Serial
2. Read Hardware/Software Versions
3. Read Current Date & Time from Watch
4. Read Battery Percentage from Watch
5. Send a Fake Call Alert

### Devices Tested

 1. Amazfit Bip U Pro
 2. Amazfit GTS 2 Mini

### Setup & Install

    pip install -r requirements.txt

### Usage

    python3 run.py <device-mac-address>
    python3 run.py AA:BB:CC:DD:EE:FF

### Screenshot

![](screenshot_1.png)
