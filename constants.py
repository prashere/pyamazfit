from enum import Enum


class BLECharacteristics(str, Enum):
    """Collection of Bluetooth Gatt Protocol Characteristics"""

    ALERT = "00000020-0000-3512-2118-0009af100700"
    BATTERY_LEVEL = "00002a19-0000-1000-8000-00805f9b34fb"
    CURRENT_TIME = "00002a2b-0000-1000-8000-00805f9b34fb"
    DEVICE_NAME = "00002a00-0000-1000-8000-00805f9b34fb"
    DEVICE_SERIAL = "00002a25-0000-1000-8000-00805f9b34fb"
    FW_REV = "00002a26-0000-1000-8000-00805f9b34fb"
    HR_MEASURE = "00002a37-0000-1000-8000-00805f9b34fb"
    HW_REV = "00002a27-0000-1000-8000-00805f9b34fb"
    SW_REV = "00002a28-0000-1000-8000-00805f9b34fb"

    def __str__(self):
        return str.__str__(self)
