def parse_time(hex_value: str) -> dict:
    if len(hex_value) < 16:
        raise ValueError("Incorrect data.")
    result = {
        "year": int(hex_value[2:4]+hex_value[0:2], 16),
        "month": int(hex_value[4:6], 16),
        "day": int(hex_value[6:8], 16),
        "hour": int(hex_value[8:10], 16),
        "minute": int(hex_value[10:12], 16),
        "second": int(hex_value[12:14], 16),
        "day_of_week": int(hex_value[14:16], 16),
    }
    return result
