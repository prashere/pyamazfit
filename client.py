import datetime

# vendor imports
from bleak import BleakClient, BleakScanner

# local imports
import helper
from constants import BLECharacteristics


class Peripheral:
    def __init__(self, address):
        self.__client = BleakClient(address)

    @property
    def address(self):
        return self.__client.address

    @property
    def is_connected(self):
        return self.__client.is_connected

    @property
    def devices(self):
        return self.__client.discovered_devices

    @staticmethod
    async def scan_devices(timeout:float=5.0):
        return await BleakScanner.discover()

    async def connect(self):
        return await self.__client.connect()

    async def disconnect(self):
        return await self.__client.disconnect()

    async def pair(self):
        return await self.__client.pair()

    async def get_device_name(self) -> str:
        device_name = await self.__client.read_gatt_char(BLECharacteristics.DEVICE_NAME)
        return "".join(map(chr, device_name))

    async def get_current_time(self) -> str:
        """
        Fetches current time from device and returns date & time as human
        readable string.
        """
        current_time = await self.__client.read_gatt_char(BLECharacteristics.CURRENT_TIME)
        current_time = current_time.hex()   # converting from bytes to hex
        parsed_time = helper.parse_time(current_time)
        datetime_str = "{year}-{month}-{date}-{hour}-{minute}-{second}"
        date_time = datetime.datetime.strptime(
            datetime_str.format(
                year=parsed_time["year"], month=parsed_time["month"],
                date=parsed_time["day"], hour=parsed_time["hour"],
                minute=parsed_time["minute"], second=parsed_time["second"]
            ),
            "%Y-%m-%d-%H-%M-%S"
        )
        return "{}".format(date_time)

    async def set_new_time(self, time_hex: str):
        await self.__client.write_gatt_char(BLECharacteristics.CURRENT_TIME,
                                            bytearray.fromhex(time_hex))

    async def get_firmware_revision(self) -> str:
        # Amazfit doesn't support firmware revision
        fw_info = await self.__client.read_gatt_char(BLECharacteristics.FW_REV)
        return "".join(map(chr, fw_info))

    async def get_hw_revision(self) -> str:
        hw_info = await self.__client.read_gatt_char(BLECharacteristics.HW_REV)
        return "".join(map(chr, hw_info))

    async def get_sw_revision(self) -> str:
        sw_info = await self.__client.read_gatt_char(BLECharacteristics.SW_REV)
        return "".join(map(chr, sw_info))

    async def get_hr_value(self):
        hr_value = await self.__client.read_gatt_char(BLECharacteristics.HR_MEASURE)
        return hr_value

    async def get_battery_level(self) -> str:
        """
        Fetches current battery level from peripheral device
        """
        batt_level = await self.__client.read_gatt_char(BLECharacteristics.BATTERY_LEVEL)
        # Amazfit returns n number of 0's where n denotes the battery percent
        return str(len(batt_level)) + "%"

    async def get_device_serial(self) -> str:
        device_serial = await self.__client.read_gatt_char(BLECharacteristics.DEVICE_SERIAL)
        return "".join(map(chr, device_serial))

    async def send_call_alert(self, caller_name: str) -> None:
        # TODO: dynamically construct data packets based on name length
        header = "000000030000000000"
        final_part = "0080{}"
        end = "000002"
        await self.__client.write_gatt_char(
            BLECharacteristics.ALERT,
            bytes.fromhex(header) + caller_name[:11].encode()
        )
        await self.__client.write_gatt_char(
            BLECharacteristics.ALERT,
            bytes.fromhex(final_part.format("01")) + caller_name[11:].encode() +
            bytes.fromhex(end)
        )
