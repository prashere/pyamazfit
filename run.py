import asyncio
import logging
import sys

from client import Peripheral


async def scan_for_devices():
    logger.info("Scanning for nearby Amazfit devices...")
    devices = await Peripheral.scan_devices()
    for idx, device in enumerate(devices):
        if "Amazfit" in device.name:
            logger.info("%d, %s, %s",
                        idx+1,
                        device.name,
                        device.rssi)
    return devices


async def main():
    devices = await scan_for_devices()
    if not devices:
        logger.info("No Devices found. Try again.")
        exit()
    choice = int(input("choose a device: "))
    device = devices[choice-1]

    # Initialize device and connect
    peripheral = Peripheral(device.address)
    logger.info("Connecting to device: %s", device.name)

    try:
        if not peripheral.is_connected:
            await peripheral.connect()
        logger.info("Connected: %s", peripheral.is_connected)

        # if peripheral.is_connected:
            # logger.info("Pairing...")
            # await peripheral.pair()

        # get device name from device
        device_name = await peripheral.get_device_name()
        logger.info("Device: %s", device_name)

        # get hardware revision string from device
        hw_rev = await peripheral.get_hw_revision()
        logger.info("Hardware Revision: %s", hw_rev)

        # get software revision string from device
        sw_rev = await peripheral.get_sw_revision()
        logger.info("Software Revision: %s", sw_rev)

        # get device serial
        device_serial = await peripheral.get_device_serial()
        logger.info("Device Serial: %s", device_serial)

        # get battery level
        batt_level = await peripheral.get_battery_level()
        logger.info("Battery: %s", batt_level)

        # get current time
        current_date_time = await peripheral.get_current_time()
        logger.info("Date & Time: %s", current_date_time)

        # FIXME: set new time is experimental and doesn't work
        # time_in_hex = "e707040e0e381105000016"
        # logger.info("Setting new time")
        # await peripheral.set_new_time(time_in_hex)

        # send call alert
        logger.info("Sending Call Alert")
        caller_name = str(input("caller name: "))
        await peripheral.send_call_alert(caller_name)
    except Exception as ex:
        logger.error(ex)
    finally:
        # disconnect
        await peripheral.disconnect()
        logger.info("Connected: %s", peripheral.is_connected)


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    console_handler.setLevel(logging.DEBUG)
    logger.addHandler(console_handler)

    asyncio.run(main())
